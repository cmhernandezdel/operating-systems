/*
 * minishell.c
 *
 * Linux shell C source code
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

#include <stddef.h>			/* Header file for NULL definition */
#include <stdio.h>			/* Header file for printf */
#include <sys/types.h>     	 	/* Header file for fork, exec, wait */
#include <unistd.h>			/* Header file for system calls read, write and close */
#include <sys/stat.h>			/* Header file for permission constants */
#include <fcntl.h> 			/* Header file for flags */
#include <string.h>			/* Header file for strtol */
#include <errno.h>			/* Header file for the errno variable */
#include <stdlib.h>			/* Header file for setenv, getenv */

#define TAM_BUFFER 1024
#define PATH_MAX 4096
#define C_SUCCESS 0
#define C_ERROR -1
#define TRUE 1
#define FALSE 0
#define EOF_SIG 0
#define DEC_BASE 10

extern int obtain_order();			/* See parser.y for description */
int checkNumber(char number[]);		/* Check if a given string is a number, returns 0 if it is a number, and -1 otherwise */
char *getFile(char path[]);			/* Get the file name from a given path */

int checkNumber(char number[]){
	int it_chk = 0;
	if (number[0] == '-') it_chk = 1; 	/* Accept negative numbers */
	
	/* Go through the array and if any character is not a number, return -1 */
	while (number[it_chk] != '\0'){
		if (number[it_chk] != '0' && number[it_chk] != '1' && number[it_chk] != '2' && number[it_chk] != '3' && number[it_chk] != '4' && number[it_chk] != '5' && number[it_chk] != '6' && number[it_chk] != '7' && number[it_chk] != '8' && number[it_chk] != '9')
	return C_ERROR;
	it_chk++;
	}
	
	return C_SUCCESS; /* If all characters are numbers, then return 0 */
}

char *getFile(char path[]){
	/* Variable declarations */
	int it_file, pos = 0, it_name, it_name2 = 0;
	char *file;
	
	/* Go through the array and if we find a '/', reset the position counter */
	for (it_file = 0; path[it_file] != '\0'; it_file++){
		pos++;
		if (path[it_file] == '/') pos = 0;
	}

	/* Allocate the memory for the string to contain the file name */
	file = (char*) malloc((pos+1)*sizeof(char)); 

	/* Copy the filename into our new array */
	it_name = it_file-pos;
	while (it_name < it_file){
	file[it_name2] = path[it_name];
	it_name++;
	it_name2++;
	}

	file[it_name2] = '\0'; /* End the string with a null character */

	return file;

}

int main(void){
	char ***argvv;
	int command_counter, num_commands, args_counter, bg, ret, fd_out, fd_err, pid, status;
	char *filev[3];
	int fd[3];
	long int acc;
	char accchar[21]; 					/* The max. value of long int has 20 digits */
	char *pointer, *file;
	char buffer[TAM_BUFFER];

	setbuf(stdout, NULL);				/* Unbuffered */
	setbuf(stdin, NULL);

	while (TRUE) 
	{	
		fprintf(stderr, "%s", "msh> ");					/* Prompt */
		ret = obtain_order(&argvv, filev, &bg);
		if (ret == EOF_SIG) break;						/* EOF */
		if (ret == C_ERROR) continue;					/* Syntax fd_err */
		num_commands = ret - 1;							/* Line */
		if (num_commands == 0) continue;				/* Empty line */
		int pipeline[2][2];



		for (command_counter = 0; command_counter < num_commands; command_counter++) 
		{
			/* Inner command: mycalc
			 * This command behaves like a mini-calculator. It allows sums and modules
			 */
			if (strcmp(argvv[0][0], "mycalc") == 0){
				
				/* Cannot use with pipes, redirections or background */
				if (num_commands != 1 || filev[0]!=NULL || filev[1]!=NULL || filev[2]!=NULL || bg!=0){
					printf("[ERROR]: Cannot use mycalc with pipes, redirections or background\n");
					break;
				}
				
				/* Check if the syntax is correct */
				if (argvv[0][1] == NULL || argvv[0][2] == NULL || argvv[0][3] == NULL || argvv[0][4] != NULL){
					printf("[ERROR] Usage: <number1> <add/mod> <number2>\n");
					break; 
					}
					
				/* Check if values are valid numbers */
				if (checkNumber(argvv[0][1]) == C_ERROR || checkNumber(argvv[0][3]) == C_ERROR){
					printf("[ERROR] Usage: <number1> <add/mod> <number2>\n");
					break;
					}
				
				char *ptr;
				/* Check if values are in range*/
				long int n1 = strtol(argvv[0][1], &ptr, DEC_BASE);
				if (errno == ERANGE){
					printf("[ERROR] Overflow\n");
					break;
					}
				long int n2 = strtol(argvv[0][3], &ptr, DEC_BASE);
				if (errno == ERANGE){
					printf("[ERROR] Overflow\n");
					break;
					}
				
				/* Add operation, which sums two numbers and updates the Acc variable accordingly */
				if (strcmp(argvv[0][2], "add") == 0){
					if (setenv("Acc", "0", 0) == C_ERROR){
						printf("[ERROR]: Error declaring the environment variable\n");
						break;
					}
					
					long int sum = n1+n2;
					pointer = getenv("Acc");
					if (pointer == NULL){
						printf("[ERROR] Cannot obtain the environment variable\n");
						break;
					}
					
					/* Environment variable update */
					acc = strtol(pointer, &ptr, DEC_BASE);
					acc += sum;
					sprintf(accchar, "%ld", acc);
					if (setenv("Acc", accchar, 1) == C_ERROR){
						printf("[ERROR]: Error declaring the environment variable\n");
						break;
					}
					
					printf("[OK] %ld + %ld = %ld, Acc = %ld\n", n1, n2, suma, acc); /* Print the result */
					break;
				}
				
				/* Mod operation, which obtains the module of two numbers */
				else if (strcmp(argvv[0][2], "mod") == 0){
					long int mod = n1 % n2;
					long int quotient = n1 / n2;
					
					printf("[OK] %ld %% %ld = %ld * %ld + %ld\n", n1, n2, n2, quotient, mod);
					break;
				}
				
				/* If there are any errors */
				else{
					printf("[ERROR] Usage: <number1> <add/mod> <number2>\n");
					break;
					}
			}
		
			/* Inner command: mybak, which works like the cp linux command */

			if (strcmp(argvv[0][0], "mybak") == 0){
				/* Cannot use with pipes, redirections or background */
				if (num_commands != 1 || filev[0]!=NULL || filev[1]!=NULL || filev[2]!=NULL || bg!=0){
					printf("[ERROR]: Cannot use mybak with pipes, redirections or background\n");
					break;
				}

				/* Check the number of arguments */
				if (argvv[0][1] == NULL || argvv[0][2] == NULL){
					printf("[ERROR] Usage: ./mybak <origen> <destination>\n");
					break;
				}

				/* Open the file */
				if ((fd[0] = open(argvv[0][1], O_RDONLY)) == C_ERROR){
					printf("[ERROR] Error opening the file\n");
					break;
				}

				char file_array[PATH_MAX];
				size_t count = TAM_BUFFER;
				ssize_t read_characters, written_characters;
				file = getFile(argvv[0][1]);
				
				strcpy(file_array, argvv[0][2]);
				strcat(file_array, "/");
				strcat(file_array, file);
				if ((fd[1] = creat(file_array, S_IRUSR | S_IWUSR)) == -1){
					printf("[ERROR] Error creating the destination file\n");
					free(file);
					break;
				}
				/* Read the file and write it in the destination file */
				do{
					if ((read_characters = read (fd[0], buffer, count)) == -1){
						printf("[ERROR] Error reading from file\n");
						free(file);
						break;
					}
					if ((written_characters = write(fd[1], buffer, read_characters)) == -1){
						perror("[ERROR] Error writing into file\n");
						free(file);
						break;
					}
				}
				while (read_characters == TAM_BUFFER)
						printf("[OK] Success copying file %s to directory %s\n", argvv[0][1], argvv[0][2]);
						free(file);
						break;
							
			}
			
			/* Simple command execution */
				if (command_counter != 0 && command_counter % 2 == 0){
					if(close(pipeline[0][0]) == -1){
						perror("[ERROR] There was a problem closing the pipe\n");
						exit(C_ERROR);
						}
					if(close(pipeline[0][1]) == -1){
						perror("[ERROR] There was a problem closing the pipe\n");
						exit(C_ERROR);
						}
				}

				if (command_counter != 1 && command_counter % 2 == 1){
					if(close(pipeline[1][0]) == -1){
						perror("[ERROR] There was a problem closing the pipe\n");
						exit(C_ERROR);
						}
					if(close(pipeline[1][1]) == -1){
						perror("[ERROR] There was a problem closing the pipe\n");
						exit(C_ERROR);
						}
				}	

			/* Only create the pipe if there is more than one command */
			if (num_commands > 1 && command_counter != num_commands-1){
			pipe(pipeline[command_counter%2]);
			}
		           
			if ((pid = fork()) == -1){
				perror("[ERROR] Error creating the child process\n");
				break;
				}

			/* Child process code segment */
 			if (pid == 0){
				
 				/* Input redirections */
				if (filev[0] != NULL && command_counter == 0){ 		/* Only redirection the input in the first command */
					fd[0] = open(filev[0], O_RDONLY);
					if (fd[0] == -1){
						perror("[ERROR] Error opening the file\n");
						exit(-1);
						}
					if (close(STDIN_FILENO) == -1){
						perror("[STDIN] Error closing standard input descriptor\n");
						exit(-1);
						}
					if(dup(fd[0]) == -1){
						perror("[ERROR] Error duplicating the file descriptor\n");
						exit(-1); 
					}
					if (close(fd[0]) == -1){
						perror("[ERROR] Error closing the file\n");
						exit(-1); 
						}
				}
			
				if (num_commands > 1){
					/* First command */
					if (command_counter == 0){        	  
						if(close(pipeline[0][0]) == -1){			
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}		
						if (close(STDOUT_FILENO) == -1){
							perror("[STDOUT] Error closing standard output descriptor\n");
							exit(-1);
							}														/* Close standard output */
						if(dup(pipeline[0][1]) == -1){
							perror("[ERROR] Error duplicating the pipe descriptor\n");
							exit(-1);							
							}	     	/* Swap standard output for pipe output */ 
						if (close(pipeline[0][1]) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
								}		/* Close the pipe */
					}
					/* Middle commands */
					if (command_counter != num_commands - 1 && command_counter != 0){
						
						if(close(pipeline[command_counter%2][0]) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}
																	/* Close the input of the current pipe, we do not need it */
						if (close(pipeline[(command_counter-1)%2][1]) == -1){
							perror("[ERROR] Error closing the pipe.\n");
							exit(-1);
							}
																	/* Same with output from the last pipe */
						if (close(STDIN_FILENO) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}										/* Close standard input */
						if(dup(pipeline[(command_counter-1)%2][0]) == -1){
							perror("[ERROR] Error duplicating the pipe\n");
							exit(-1);
							}										/* Connect the first pipe as input */
						if(close(pipeline[(command_counter-1)%2][0])){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}
																	/* Close the pipe */
						if(close(STDOUT_FILENO) == -1){
							perror("[STDOUT] Error closing the standard output\n");
							exit(-1);
							}						/* Close the standard output */
						if(dup(pipeline[command_counter%2][1]) == -1){
							perror("[ERROR] Error duplicating the pipe\n");
							exit(-1);
							}				         /* Connect pipe as output */ 
						if (close(pipeline[command_counter%2][1]) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							} 		/* Close the pipe */
						 									
					} 
					/* Final command */
					if (command_counter == num_commands - 1){

						if (close(pipeline[(command_counter-1)%2][1]) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}							/* Close output from the last pipe */
						if (close(STDIN_FILENO) == -1) {
							perror("[STDIN] Error closing the standard input\n");
							exit(-1);
							}							/* Close standard input */
						if (dup(pipeline[(command_counter-1)%2][0]) == -1){
							perror("[ERROR] Error duplicating the pipe\n");
							exit(-1);
							}  							/* Swap standard input with last pipe input */
						if (close(pipeline[(command_counter-1)%2][0]) == -1){
							perror("[ERROR] Error closing the pipe\n");
							exit(-1);
							}							/* Close the pipe */
						
					}
				
				}
			
				/* Input/Output redirections */
				if (filev[1] != NULL && command_counter == num_commands-1){	/* Only redirect output in the last command */
					fd[1] = open(filev[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
					if (fd[1] == -1){
						perror("[ERROR] Error opening the file\n");
						exit(-1); 
						}
					if (close(STDOUT_FILENO) == -1){
						perror("[STDOUT] Error closing the standard output\n");
						exit(-1); 
						}
					if(dup(fd[1]) == -1){
						perror("[ERROR] Error duplicating the file\n");
						exit(-1); 
					}
					if (close(fd[1]) == -1){
						perror("[ERROR] Error closing the file\n");
						exit(-1); 
						}
				}	
	
				if (filev[2] != NULL && command_counter == num_commands-1){	/* Only redirect error output in the last command */
					fd[2] = open(filev[2], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
					if (fd[2] == -1){
						perror("[ERROR] Error opening the file\n");
						exit(-1); 
						}
					if (close(STDERR_FILENO) == -1){
						perror("[STDERR] Error closing the error output\n");
						exit(-1); 
						}
					if(dup(fd[2]) == -1){
						perror("[ERROR] Error duplicating the file\n");
						exit(-1); 
					}
					if (close(fd[2]) == -1){
						perror("[ERROR] Error closing the file\n");
						exit(-1); 
						}
				}
 					execvp(argvv[command_counter][0], &argvv[command_counter][0]);
 					perror("[ERROR] The program does not exist\n");
					return -1;
 			} /* End of child process */
 				
				/* Close the pipe in the parent process */
			else{	
				if (command_counter == num_commands - 1){ /* Only in the last oommand. The rest of commands are controlled by the pipe */
					if (num_commands > 1 && num_commands % 2 == 1){
							if (close(pipeline[1][0]) == -1){
								perror("[ERROR] Error closing the pipe\n");
								exit(-1);
								}
							if (close(pipeline[1][1]) == -1){
								perror("[ERROR] Error closing the pipe\n");
								exit(-1);
								}
						}

					if (num_commands > 1 && num_commands % 2 == 0){
							if (close(pipeline[0][0]) == -1){
								perror("[ERROR] Error closing the pipe\n");
								exit(-1);
								}
							if (close(pipeline[0][1]) == -1){
								perror("[ERROR] Error closing the pipe\n");
								exit(-1);
								}
						}
					/* If not executed in background mode (&) */
					if(bg != 1){
					/* Wait for all child processes, this one and all the others so they don't end as zombie processes */
					while(waitpid(-1, &status, 0) > 0) continue; /* Reap all the corpses */
					}
			
					else{
						printf("[%d]\n",pid);           /* Print child PID */
					}
				}
					
			}			
			
			
		}  //end for

	} //end while 

	return 0;

} //end main

