/*
 * factory_manager.c
 *
 * Aditional functions
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stddef.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>

#define BUFFER_SIZE 1024
#define NUM_ARGS 6
#define ARGS_LENGTH 80

/* Checks if a string is a number */
int checkNumber(char number[]){
	int it_chk = 0;
	while (number[it_chk] != '\0' && number[it_chk] != '\n'){
		if (number[it_chk] != '0' && number[it_chk] != '1' && number[it_chk] != '2' && number[it_chk] != '3' && number[it_chk] != '4' && number[it_chk] != '5' && number[it_chk] != '6' && number[it_chk] != '7' && number[it_chk] != '8' && number[it_chk] != '9')
	return -1;
	it_chk++;
	}
	
	return 0; 
}

/* Copy to buffer so we can read files of any size */
int copyArray(char *from, char *to, int iteration, int read_characters){
    int it_copyarray;
    int it_copyarray2 = 0;
    for(it_copyarray = BUFFER_SIZE*iteration; it_copyarray < (BUFFER_SIZE*(iteration+1) - (BUFFER_SIZE-read_characters)); it_copyarray++){
        to[it_copyarray] = from[it_copyarray2];
        it_copyarray2++;
    }
    return 0;
}

int main (int argc, const char * argv[]){
	
	/* Variable declarations */
	int fd, process_max, file_size, it_extract, it_p, it_n, it_fork, it_arguments, it_allow, it_sem, it_params, status, real_processes = 0, it_readcharacters = 0;
	char buffer[BUFFER_SIZE];
	ssize_t read_characters;
    char *token, *ptr;
	
    /* Check number of arguments */
	if (argc != 2){
		perror("[ERROR][factory_manager] Invalid file.\n");
		return -1;
	}
    	
    /* Open and read file */
	if ((fd = open(argv[1], O_RDONLY)) == -1){          
		perror("[ERROR][factory_manager] Invalid file.\n");
		return -1;
	}
    
    /* Run lseek at the end so we can know the file size */
    if ((file_size = lseek(fd, 0, SEEK_END)) <= 0){
        perror("[ERROR][factory_manager] Invalid file.\n");
        return -1;
    }     

    /* Reset file pointer */
    if(lseek(fd, 0, SEEK_SET) == -1){
        perror("[ERROR][factory_manager] Invalid file.\n");
        return -1;
    }                     
    char text[file_size];                 

    do{
        /* Read from file to buffer */
        if((read_characters = read(fd, buffer, BUFFER_SIZE)) == -1){
            perror("[ERROR][factory_manager] Invalid file.\n");
            return -1;
        }                
        copyArray(buffer, text, it_readcharacters, read_characters);    /* And then to our array */
        it_readcharacters++;                                            /* Update iterator */
    }
    while (read_characters == BUFFER_SIZE);                             /* Repeat while not EOF */
    
    /* Gathering of information from the file */
    token = strtok(text, " ");                /* First character is number of processes */
    if (checkNumber(token) == -1){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If not a number or lesser or equal than 0, error */
            return -1;
        }
    process_max = (int)strtol(token, &ptr, 10); /* Cast to int */
    if (process_max <= 0){
        perror("[ERROR][factory_manager] Invalid file.\n"); /* If not a number or lesser or equal than 0, error */
        return -1;
       }


    int process_id[process_max], max_size[process_max], n_elements[process_max]; 

    /* 0 index for first process, 1 for second... */

    for (it_extract = 0; it_extract < process_max; it_extract++){
        token = strtok(NULL, " ");
        if (token == NULL) break;
        if (checkNumber(token) == -1){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If not a number */
            return -1;
        }
        process_id[it_extract] = (int) strtol(token, &ptr, 10);
        token = strtok(NULL, " ");
        if (token == NULL || checkNumber(token) == -1){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If we don't have the maximum size of the queue */
            return -1;
        }
        max_size[it_extract] = (int)strtol(token, &ptr, 10);
        if (max_size[it_extract] <= 0 || errno == ERANGE){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If error */
            return -1;
        }
        token = strtok(NULL, " ");
        if (token == NULL || checkNumber(token) == -1){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If we don't have the number of elements to produce */
            return -1;
        }
        n_elements[it_extract] = (int) strtol(token, &ptr, 10);
        if (n_elements[it_extract] <= 0 || errno == ERANGE){
            perror("[ERROR][factory_manager] Invalid file.\n"); /* If error */
            return -1;
        }
        real_processes++;
    }

    token = strtok(NULL, " ");
    if (token != NULL){
        perror("[ERROR][factory_manager] Invalid file.\n"); /* If there are more processes than maximum */
        return -1;
	}

    /* Close file */
    if (close(fd) == -1){
		perror("[ERROR][factory_manager] Invalid file.\n");
		return -1;
	}
	
	for(it_p = 0; it_p < real_processes; it_p++){
		for(it_n = 0; it_n < real_processes; it_n++){
			if(it_p != it_n){
				if(process_id[it_p] == process_id[it_n]){
					perror("[ERROR][factory_manager] Invalid file.\n");
					return -1;
					}
				}
			}
		}
	
    /* Create semaphores and children processes */
    sem_t *semaphore[real_processes];
    char sem_names[real_processes][ARGS_LENGTH];
        for(it_sem = 0; it_sem < real_processes; it_sem++){
            sprintf(sem_names[it_sem], "%d", process_id[it_sem]);                     /* Call each semaphore like its process id */
            sem_unlink(sem_names[it_sem]);                                            /* Unlink just in case they were in memory from previous executions */
            semaphore[it_sem] = sem_open(sem_names[it_sem], O_CREAT, S_IRWXU, 0);     /* Open semaphores and set to 0 */
        }

    /* Children processes */
    int pid[real_processes];
    char *args[real_processes][NUM_ARGS];
    char params[real_processes][NUM_ARGS-3][ARGS_LENGTH];


    /* Workaround so sprintf works */
    for(it_params = 0; it_params < real_processes; it_params++){
        sprintf(params[it_params][0], "%d", process_id[it_params]); /* Params[0] = process_id */
        sprintf(params[it_params][1], "%d", max_size[it_params]);   /* Params[1] = max_size */
        sprintf(params[it_params][2], "%d", n_elements[it_params]); /* Params[2] = n_elements */
    }
    
    /* Create array of arguments */
    for(it_arguments = 0; it_arguments < real_processes; it_arguments++){
        args[it_arguments][0] = "./process";                            /* Argumento 0: process name */
        args[it_arguments][1] = params[it_arguments][0];                /* Argumento 1: process_id */
        args[it_arguments][2] = sem_names[it_arguments];                /* Argumento 2: semaphore name */
        args[it_arguments][3] = params[it_arguments][1];                /* Argumento 3: max_size */
        args[it_arguments][4] = params[it_arguments][2];                /* Argumento 4: n_elements */
        args[it_arguments][5] = NULL;                                   /* Argumento 5: NULL */
    }
    
    /* Create processes */
    for (it_fork = 0; it_fork < real_processes; it_fork++){
        pid[it_fork] = fork();
        if (pid[it_fork] == 0){
            execvp(args[it_fork][0], args[it_fork]);
            return -1;
        }
        else{
            printf("[OK][factory_manager] Process_manager with id %d has been created.\n", process_id[it_fork]);
        }
    }
    /* Let them run in order */    
    for (it_allow = 0; it_allow < real_processes; it_allow++){
        sem_post(semaphore[it_allow]);
        if (waitpid(-1, &status, 0) == -1){
            fprintf(stderr, "[ERROR][factory_manager] Process_manager with id %d has finished with errors.\n", process_id[it_allow]);
        }
        else{
            printf("[OK][factory_manager] Process_manager with id %d has finished.\n", process_id[it_allow]);
        }
        sem_close(semaphore[it_sem]);
    }
    
    /* Workaround for strtok */
    if (real_processes == 0){
    	perror("[ERROR][factory_manager] Invalid file.\n");
    	return -1;
    }
    printf("[OK][factory_manager] Finishing.\n");
    
	return 0;
}
