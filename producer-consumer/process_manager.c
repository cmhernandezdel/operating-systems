/*
 * process_manager.c
 *
 * Creates and joins the threads
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stddef.h>
#include <pthread.h>
#include "queue.h"
#include <semaphore.h>
#include <errno.h>

#define NUM_THREADS 2

pthread_mutex_t mutex;
int argsThreads[3];
pthread_cond_t no_lleno, no_vacio;
int num_elementos = 0;                // Number of elements produced or consumed

void *ProducerFunction(){

    int it_producer;
    struct element elm[argsThreads[1]];
    for(it_producer = 0; it_producer < argsThreads[1]; it_producer++){
        pthread_mutex_lock(&mutex);                 // Lock the mutex
        while(num_elementos == argsThreads[2])      // If we reached the maximum of elements that we can produce, release mutex
            pthread_cond_wait(&no_lleno, &mutex);
        elm[it_producer].num_edition = it_producer;
        elm[it_producer].id_belt = argsThreads[0];
        queue_put(&elm[it_producer]);
        num_elementos++;
        pthread_cond_signal(&no_vacio);             // Send signal of queue not empty
        pthread_mutex_unlock(&mutex);               // And release the mutex
    }
    pthread_exit(NULL);                             
}

void *ConsumerFunction(){
    
    int it_consumer;
    struct element *elm[argsThreads[1]];
    for(it_consumer = 0; it_consumer < argsThreads[1]; it_consumer++){
        pthread_mutex_lock(&mutex);                 // Lock the mutex
        while(num_elementos == 0)                   // If there are no elements, release mutex
            pthread_cond_wait(&no_vacio, &mutex);
        elm[it_consumer] = queue_get();          
        num_elementos--;
        pthread_cond_signal(&no_lleno);             // Send signal of queue not full
        pthread_mutex_unlock(&mutex);               // Release mutex
    }
    pthread_exit(NULL);                            
}

int main (int argc, const char * argv[]){

    printf("[OK][process_manager] Process_manager with id: %s waiting to produce %s elements.\n", argv[1], argv[4]);
    // Variable declarations
    int process_id, max_size, n_elements;
    char *ptr;
    const char *sem_name = argv[2];
    sem_t *semaphore;
    pthread_t consumer, producer;
    if((semaphore = sem_open(sem_name, O_RDWR)) == SEM_FAILED){
        perror("[ERROR][process_manager] Arguments not valid.\n");
        return -1;
    }

    // Get arguments and check for errors
    if((process_id = (int)strtol(argv[1], &ptr, 10)) == 0 && errno == EINVAL){
        perror("[ERROR][process_manager] Arguments not valid.\n");
        return -1;
    }
    if((n_elements = (int)strtol(argv[4], &ptr, 10)) == 0 && errno == EINVAL){
        perror("[ERROR][process_manager] Arguments not valid.\n");
        return -1;
    }
  
    if((max_size = (int)strtol(argv[3], &ptr, 10)) == 0 && (errno == EINVAL)){
        perror("[ERROR][process_manager] Arguments not valid.\n");
        return -1;
    }

    argsThreads[0] = process_id;
    argsThreads[1] = n_elements;
    argsThreads[2] = max_size;


    // Wait until parent sends signal
    if(sem_wait(semaphore) == -1){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }

    // Creation and initialization of queue
    if(queue_init(max_size) == -1){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    printf("[OK][process_manager] Belt with id: %d has been created with a maximum of %d elements.\n", process_id, max_size);

    // Creation of consumer and producer
    if(pthread_mutex_init(&mutex, NULL) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    if(pthread_cond_init(&no_lleno, NULL) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    if(pthread_cond_init(&no_vacio, NULL) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    if(pthread_create(&producer, NULL, ProducerFunction, NULL) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    if(pthread_create(&consumer, NULL, ConsumerFunction, NULL) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }

    // Wait for children to end
    pthread_join(producer, NULL);
    pthread_join(consumer, NULL);
    printf("[OK][process_manager] Process_manager with id: %d has produced %d elements.\n", process_id, n_elements);
    if(pthread_mutex_destroy(&mutex) != 0){
        fprintf(stderr, "[ERROR][process_manager] There was an error executing process_manager with id: %d.", process_id);
        return -1;
    }
    sem_close(semaphore);
    queue_destroy();
	return 0;
	
}
