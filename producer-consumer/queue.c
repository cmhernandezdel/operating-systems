/*
 * queue.c
 *
 * Queue implementation
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "queue.h"



// Struct node, contains an element and a pointer to the next one
struct node{
    struct element elm;
    struct node *next;
};

struct node *front, *rear, *temp;
struct element rtn;

int n_elementos = 0;
int q_size = 0;

// To create a queue
int queue_init(int size){
    if (size < 0) return -1;
    front = NULL;
    rear = front;
    q_size = size;
	return 0;
}


// To enqueue an element
int queue_put(struct element* x) {
    if (queue_empty()){
        rear = (struct node *)malloc(1*sizeof(struct node));
        rear->next = front; /* Cola circular */
        rear->elm = *x;
        rear->elm.last = 1;
        front = rear;
        n_elementos++;
        printf("[OK][queue] Introduced element with id: %d in belt %d.\n", rear->elm.num_edition, rear->elm.id_belt);
        return 0;
    }
    else if (!queue_full()){
        temp=(struct node *)malloc(1*sizeof(struct node));
        rear->elm.last = 0;  
        rear->next = temp;
        temp->elm = *x;
        temp->elm.last = 1;
        temp->next = front; // Circular queue
        rear = temp;
        n_elementos++;
        printf("[OK][queue] Introduced element with id: %d in belt %d.\n", rear->elm.num_edition, rear->elm.id_belt);
        return 0;
    }
	else{
        fprintf(stderr, "[ERROR][queue] There was an error while using queue with id: %d.\n", rear->elm.id_belt);
        return -1;
    } 
    
}


// To dequeue an element
struct element* queue_get(void) {
    if (queue_empty()){
        fprintf(stderr, "[ERROR][queue] There was an error while using queue with id: %d.\n", rear->elm.id_belt);
        return NULL;
    }
    else{
        rtn = front->elm;
        rear->next = front->next;
        free(front);
        front = rear->next; // Circular queue
        n_elementos--;
        printf("[OK][queue] Obtained element with id: %d in belt %d\n", rtn.num_edition, rtn.id_belt);
        return &rtn;
    }
}


// To check queue state
int queue_empty(void){
	if (n_elementos == 0) return 1;
    else return 0;
}

int queue_full(void){
	if (n_elementos == q_size) return 1;
    else return 0;
}

// To destroy the queue and free the resources
int queue_destroy(void){
    if (queue_empty()){
    q_size = 0;
    n_elementos = 0;
	return 0;
    }
    return -1;
}
