<h2> Operating systems </h2>
Contains exercises from Operating Systems course at Universidad Carlos III de Madrid.

Feel free to view, use and redistribute this code in any way.

<h3> Bash functions </h3>
Implementation of bash functions <code>ls</code>, <code>cat</code> and a fork of <code>cat</code>.
This problem was solved using C in 2016.<br>
For more information on the topic, see <a href="http://man7.org/linux/man-pages/man1/ls.1.html">ls</a> and <a href="http://man7.org/linux/man-pages/man1/cat.1.html">cat</a>.

<h3> Minishell </h3>
Implementation of a complete functional terminal emulator for Linux. It also has additional functions like a very simple calculator.
This problem was solved using C in 2016.

<h3> Consumer and producer problem </h3>
Implementation of the consumer-producer problem using multi threading and queues.
This problem was solved using C in 2016.<br>
For more information on the consumer-producer problem, see <a href="https://en.wikipedia.org/wiki/Producer%E2%80%93consumer_problem">the wikipedia article</a>.<br> For more information on queues, see <a href="https://gitlab.com/cmhernandezdel/data-structures-and-algorithms/tree/master/Queues">my project on the topic</a>.