/*
 * mysize.c
 *
 * Linux own 'cat with size' like command C source code
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

#include <stdio.h>		/* Header file for printf */
#include <sys/types.h>		/* Header file for system call fstat */
#include <sys/stat.h>		/* Header file for constants: S_IRWXU, etc. */
#include <fcntl.h>		/* Header file for system call fcntl */
#include <dirent.h> 		/* Header file for system calls opendir, closedir, readdir... */
#include <unistd.h>		/* Header file for system calls read, write and close */

#define MAX_ARGUMENTS 1
#define C_ERROR -1
#define C_SUCCESS 0

int main(int argc, char *argv[]){

	/* Check the number of arguments */
	if(argc > MAX_ARGUMENTS){
		printf("[ERROR]: Incorrect syntax. Usage: ./mysize\n");
		return C_ERROR;
	}
	
	/* Variable declarations */
	char buffer[PATH_MAX];
	char *path;
	DIR *directory;
	struct dirent *estr;
	
	if((path=getcwd(buffer, PATH_MAX)) == NULL){
		printf("[ERROR]: Error getting the directory name\n");
		return C_ERROR;
	}
	
	/* Open the directory and check for errors */
	if ((directory = opendir(path)) == NULL){
		printf("[ERROR]: Error opening the directory. The directory does not exist\n");
		return C_ERROR;
	}
	
	/* Read the directory and the size for each entry */
	estr = readdir(directory);
	
	while (estr != NULL){
		if (estr->d_type == DT_REG){
			int fd = open(estr->d_name, O_RDONLY);
			if (fd == C_ERROR){
				printf("[ERROR]: Error opening the file\n");
				return C_ERROR;
				}
			off_t lenght = lseek(fd, 0, SEEK_END);
			if (lenght == C_ERROR){
				printf("[ERROR]: Error obtaining the size\n");
				return C_ERROR;
				}
			close (fd);
			printf("%s\t%ld \n", estr->d_name, lenght);
			}
		estr = readdir(directory);
		}
		
	/* Close the directory and exit */
	if(closedir(directory) == C_ERROR){
		printf("[ERROR]: Error closing the directory\n");
		return C_ERROR;
		}
	return C_SUCCESS;

}
