/*
 * mycat.c
 *
 * Linux cat command C source code
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */
 
#include <stdio.h>			/* Header file for printf */
#include <sys/types.h>			/* Header file for system call open */
#include <sys/stat.h>			/* Header file for constants: S_IRWXU, etc. */
#include <fcntl.h>			/* Header file for constants: O_CREAT, etc. */
#include <unistd.h>			/* Header file for system calls read, write y close */

#define BUFFER_SIZE 1024
#define C_ERROR -1
#define C_SUCCESS 0

int main(int argc, char *argv[]){

	/* Check if the number of arguments is incorrect */
	if (argc != 2){
		printf("[ERROR]: Incorrect syntax. Usage: ./mycat file\n");
		return C_ERROR;
	}

	/* Variable declarations */
        char buffer[BUFFER_SIZE];
        ssize_t read_characters, written_characters;
		int fd;
	
	/* Open the file passed as argument and check for errors */
	if( (fd = open(argv[1], O_RDONLY)) == C_ERROR){
		printf("[ERROR]: File does not exist.\n");
		return C_ERROR;
	}
	
	/* Read the file and print it */
	do{
		if( (read_characters = read(fd, buffer, BUFFER_SIZE)) == C_ERROR){
			printf("[ERROR]: Could not read the file.\n");
			return C_ERROR;
		}
		if( (written_characters = write(STDOUT_FILENO, buffer, read_characters)) == C_ERROR){
			printf("[ERROR]: Could not print the result.\n");
			return C_ERROR;
		}
	}
	while(read_characters == BUFFER_SIZE);
	
	/* Close the file when finished */
	if (close(fd) == C_ERROR){
		printf("[ERROR]: Could not close the file.\n");
		return C_ERROR;
		}
	
	return C_SUCCESS;
	
}

