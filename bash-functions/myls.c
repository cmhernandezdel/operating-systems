/*
 * myls.c
 *
 * Linux ls command C source code
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

#include <stdio.h>		/* Header file for printf */
#include <unistd.h>		/* Header file for system calls read, write y close */
#include <sys/types.h>		/* Header file for system calls opendir, readdir y closedir */
#include <dirent.h>		/* Header file for struct dirent */
#include <string.h>		/* Header file for string related functions */

#define C_ERROR -1
#define C_SUCCESS 0
#define MAX_ARGS 2
int main(int argc, char *argv[]){

	/* Check if the number of arguments is incorrect */
	if (argc > MAX_ARGS){
		printf("[ERROR]: Incorrect syntax. Usage: ./myls OR ./myls directory\n");
		return C_ERROR;
	}
	
	/* Variable declarations */
	char *path;
	DIR *directory;
	struct dirent *estr;
	       
	if(argc == (MAX_ARGS - 1)){                                            
		char buffer[PATH_MAX];
		if((path=getcwd(buffer, PATH_MAX)) == NULL){
			printf("[ERROR]: Error getting the directory name\n");
			return C_ERROR;
		}
	}
	
	if(argc == MAX_ARGS){ 
		path=argv[MAX_ARGS - 1];
	}
	
	/* Open the directory and check for errors */
	if ((directory=opendir(path)) == NULL){
		printf("[ERROR]: Error opening the directory. The directory does not exist\n");
		return C_ERROR;
	}		
		
	/* Read the directory */
	while((estr=readdir(directory)) != NULL){  
		printf("%s \n", estr->d_name);       
	}
	
	/* Close the directory and exit */
	if (closedir(directory) == C_ERROR){
		printf("[ERROR]: Error closing the directory\n");
		return C_ERROR;
		}                        
	
	return C_SUCCESS;
}

